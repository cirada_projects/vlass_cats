Code to tidy up component table, produce host ID table, and finalise the catalogue
----------------------------------------------------------------------------------
----------------------------------------------------------------------------------


The first pipeline would have produced two files for catalogue say filename1.csv and for subtiles named filename2.csv


1. Step 1:
python3 component_table/vlass_compcat_vlad_stage2_yg.py test_data/filename1.csv  test_data/filename2.csv  other_survey_data/CATALOG41.FIT other_survey_data/first_14dec17.fits

This will create a file called VLASS_components.csv

2. Step 2:
python3 host_table/vlass_iso_and_cd_finding_v2.py VLASS_components.csv

This will create a file named VLASS_source_candidates.csv

2. Step 3:

python3 host_table/vlass_uw_lr_v2.py VLASS_source_candidates.csv other_survey_data/unWISE_coad_directory.csv

4. Step 4:

python3 host_table/stack_matches_v1.py VLASS_source_candidates.csv VLASS_components.csv

5. Step 5:

python3 finalise_cat/VLASSQL1CIR_catalogue_finalise.py VLASS_table1_components.csv VLASS_table2_hosts.csv test_data/filename2

